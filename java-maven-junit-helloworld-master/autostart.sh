#!/bin/sh

cd /home/app
java -Dspring.profiles.active=dev -Dserver.port=$PORT -jar $JAR 2>&1 | tee /opt/app.log
